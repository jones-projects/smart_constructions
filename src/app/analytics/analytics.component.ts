import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-analytics',
    templateUrl: './analytics.component.html',
    styleUrls: ['./analytics.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class AnalyticsComponent implements OnInit {

    public labelData: any;
    public topActions: any[];
    public topIncidents: any[];

    constructor() {
        this.topActions = [
            {
                'id': 1,
                'details': 'Accident Avoidance',
                'type': 'criticle',
                'value':33,
                'utilizationNotificationIcon': 'Accident_Avoidance.svg'
            }, {
                'id': 2,
                'details': 'Digital Communication',
                'type': '',
                'value':17,
                'utilizationNotificationIcon': 'Digital Communication.svg'
            },
            {
                'id': 3,
                'details': 'Idle Time Detection',
                'type': '',
                'value':10,
                'utilizationNotificationIcon': 'Idle Time Detection.svg'
            },
            {
                'id': 4,
                'details': 'Reduction in Dwell Time',
                'type': '',
                'value':10,
                'utilizationNotificationIcon': 'Reduction in Dwell Time.svg'
            },
            {
                'id': 5,
                'details': 'Reduction in Equipment Tracking',
                'type': '',
                'value':5,
                'utilizationNotificationIcon': 'Reduction in Equipment Tracking.svg'
            }
        ];
        this.labelData = [
            { data: [65, 59, 80, 81, 56, 55, 40], label: 'Productivity Score' },
            { data: [28, 48, 40, 19, 86, 27, 90], label: 'Resolved Alerts' },
            { data: [180, 480, 770, 90, 1000, 270, 400], label: 'Unresolved Alerts', yAxisID: 'y-axis-1' }
        ];

        this.topIncidents = [
            {
                'id': 1,
                'details': 'Proximity to Accident zone',
                'type': '',
                'value':25,
                'utilizationNotificationIcon': 'Proximity to Accident  Zone.svg'
            }, {
                'id': 2,
                'details': 'Protective Helmet Missing',
                'type': '',
                'value':16,
                'utilizationNotificationIcon': 'Protective Helmet Missing.svg'
            },
            {
                'id': 3,
                'details': 'Safety Gear Missing',
                'type': '',
                'utilizationNotificationIcon': 'Safety Gear Missing.svg'
            },
            {
                'id': 4,
                'details': 'Entry into Unauthorized Area',
                'type': '',
                'utilizationNotificationIcon': 'Entry.svg'
            },
            {
                'id': 5,
                'details': 'Health Risk Detected',
                'type': '',
                'utilizationNotificationIcon': 'Health.svg'
            }
        ];
    }

    

    ngOnInit() {
        // this.labelData = [
        //     { data: [65, 59, 80, 81, 56, 55, 40], label: 'Productivity Score' },
        //     { data: [28, 48, 40, 19, 86, 27, 90], label: 'Resolved Alerts' },
        //     { data: [180, 480, 770, 90, 1000, 270, 400], label: 'Unresolved Alerts', yAxisID: 'y-axis-1' }
        // ];
    }

}