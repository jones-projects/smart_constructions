import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalyticsComponent } from './analytics.component';
import { BarChartModule } from '../common/bar.chart/bar.chart.module';
import { LineChartModule } from '../common/line.chart/line.chart.module';
import { ProgressChartModule } from '../common/progress.chart/progress.chart.module';
import { ListModule } from '../list/list.module';

@NgModule({

	imports: [
		CommonModule,
		BarChartModule,
		LineChartModule,
		ProgressChartModule,
		ListModule,
	],
	declarations: [
		AnalyticsComponent
	],
	exports: [
		AnalyticsComponent
	]

})

export class AnalyticsModule { }