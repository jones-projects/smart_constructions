import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LiveEventsComponent } from './live.events/live.events.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { UtilizationComponent } from './utilization/utilization.component';
import { SensorAlertsComponent } from './sensor.alerts/sensor.alerts.component';
import { OperationsComponent } from './operations/operations.component';
import { LoginComponent } from './login/login.component';
import { SecureComponent } from './secure/secure.component';

export const SECURE_ROUTES: Routes = [
    {
        path: 'live-events',
        component: LiveEventsComponent
    },
    {
        path: 'sensor-alerts',
        component: SensorAlertsComponent
    },
    {
        path: 'analytics',
        component: AnalyticsComponent
    },
    {
        path: 'utilization',
        component: UtilizationComponent
    },
    {
        path: 'operations',
        component: OperationsComponent
    }
];


const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: '', component: SecureComponent, data: { title: 'Secure Views' }, children: SECURE_ROUTES },
    { path: '**', redirectTo: '/login' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }