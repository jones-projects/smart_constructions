import { Component, ViewEncapsulation } from '@angular/core';
import { AppService } from './service/app.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class AppComponent {
    
    constructor(private appService: AppService) { 
        
    }

}
