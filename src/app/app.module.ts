import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LiveEventsComponent } from './live.events/live.events.component';
import { SensorAlertsComponent } from './sensor.alerts/sensor.alerts.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { UtilizationComponent } from './utilization/utilization.component';
import { OperationsComponent } from './operations/operations.component';
import { ListComponent } from './list/list.component';

import { AppRoutingModule } from './app-routing.module';
import { MasterHeaderModule } from './master.header/master.header.module';
import { UtilizationModule } from './utilization/utilization.module';
import { SensorAlertsModule } from './sensor.alerts/sensor.alerts.module';
import { OperationsModule } from './operations/operations.module';
import { LiveEventsModule } from './live.events/live.events.module';
import { ListModule } from './list/list.module';
import { AnalyticsModule } from './analytics/analytics.module';
import { ProgressChartModule } from './common/progress.chart/progress.chart.module';
import { LineChartModule } from './common/line.chart/line.chart.module';
import { BarChartModule } from './common/bar.chart/bar.chart.module';
import { ChartsModule } from 'ng2-charts';
import { OverlayModule } from './common/overlay/overlay.module';
import { LoginComponent } from './login/login.component';
import { SecureComponent } from './secure/secure.component';
import { AppService } from './service/app.service';



const APP_MODULES = [
    UtilizationModule,
    SensorAlertsModule,
    OperationsModule,
    MasterHeaderModule,
    LiveEventsModule,
    ListModule,
    AnalyticsModule,
    ProgressChartModule,
    LineChartModule,
    BarChartModule,
    OverlayModule,
];

const EXTERNAL_LIBRARY_MODULE = [

];

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        SecureComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        MasterHeaderModule,
        CommonModule,
        ChartsModule,
        FormsModule,
        
        APP_MODULES,
    ],
    providers: [AppService],
    bootstrap: [AppComponent]
})
export class AppModule { }
