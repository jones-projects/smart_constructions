import { Component, OnInit, Input } from '@angular/core';
import { ChartOptions, ChartDataSets, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label, Color } from 'ng2-charts';

@Component({
    selector: 'app-bar-chart',
    templateUrl: './bar.chart.component.html',
    styleUrls: ['./bar.chart.component.scss']
})
export class BarChartComponent implements OnInit {

    @Input() barMultiDataProp: boolean;
    @Input() utilizationProp: boolean;

    public barChartOptions: ChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        // We use these empty structures as placeholders for dynamic theming.
        scales: { xAxes: [{}], yAxes: [{}] },
        plugins: {
            datalabels: {
                anchor: 'end',
                align: 'end',
            }
        }
    };
    public barChartSingleDataOptions: ChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        // We use these empty structures as placeholders for dynamic theming.
        scales: {
            xAxes: [{}], yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Hours',

                }
            }]
        },
        plugins: {
            datalabels: {
                anchor: 'end',
                align: 'end',
            }
        }
    };
    public barChartMultiDataOptions: ChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        // We use these empty structures as placeholders for dynamic theming.
        scales: {
            xAxes: [{}], yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'No. Of workers'
                }
            }]
        },
        plugins: {
            datalabels: {
                anchor: 'end',
                align: 'end',
            }
        }
    };
    public barChartMultiColors: Color[] = [
        { backgroundColor: '#ef89a1' },
        { backgroundColor: '#acd37e' },
    ]
    public barChartSingalColors: Color[] = [
        { backgroundColor: '#a4dbe4' }
    ]

    public barChartMultiLabels: Label[] = ['Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'];
    public barChartSingleLabels: Label[] = ['Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'];
    // utilization
    public barChartUtilLabels: Label[] = ['Digging Crew', 'Electricions', 'Plumbing', 'Masonry', 'Pointing', 'Welding', 'Inspection', 'Planning Crew', 'Supervisions', 'Architects', 'Construction Crew', 'Crane Operations'];
    public barChartType: ChartType = 'bar';
    public barChartLegend = true;
    public barChartPlugins = [pluginDataLabels];

    public barChartMultiData: ChartDataSets[] = [
        { data: [20, 22, 35, 15, 19, 12, 28], label: 'Workers Involved' },
        { data: [77, 89, 82, 95, 86, 65, 90], label: '' }
    ];

    public barChartSingalData: ChartDataSets[] = [
        { data: [580, 600, 550, 610, 640, 570, 635, 650, 710, 650, 680, 100], label: 'Hours Saved' }
    ];

    public barChartUtilSingalData: ChartDataSets[] = [
        { data: [81, 92, 79, 91, 88, 80, 90, 82, 89, 90, 92, 93, 0], label: 'Resource  Utilization' }
    ];

    constructor() { }

    ngOnInit() {
    }

    // events
    public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
        console.log(event, active);
    }

    public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
        console.log(event, active);
    }

    // public randomize(): void {
    //     // Only Change 3 values
    //     const data = [
    //         Math.round(Math.random() * 100),
    //         59,
    //         80,
    //         (Math.random() * 100),
    //         56,
    //         (Math.random() * 100),
    //         40];
    //     this.barChartData[0].data = data;
    // }

}
