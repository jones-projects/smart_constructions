import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label, Color, BaseChartDirective } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';

@Component({
    selector: 'app-line-chart',
    templateUrl: './line.chart.component.html',
    styleUrls: ['./line.chart.component.scss']
})
export class LineChartComponent implements OnInit {

    private topLabelValue: any;
    public lineChartDataInfo: any;

    @Input() set headerFirstLabelProp(value: any){
        this.lineChartDataInfo = [...value];
    };
    public lineChartData: ChartDataSets[] = [
        { data: [65, 70, 74, 60, 68, 77, 80, 69], label: 'Series A' },
        { data: [28, 48, 40, 35, 25, 33, 39, 28], label: 'Series B' },
        { data: [10, 12, 15, 13, 17, 15, 18, 10], label: 'Series C', yAxisID: 'y-axis-1' }
    ];
    public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    public lineChartOptions: (ChartOptions) = {
        responsive: true,
        scales: {
            // We use this empty structure as a placeholder for dynamic theming.
            xAxes: [{}],
            yAxes: [
                {
                    id: 'y-axis-0',
                    position: 'left',
                },
                {
                    id: 'y-axis-1',
                    position: 'right',
                    gridLines: {
                        color: 'rgba(255,0,0,0.3)',
                    },
                    ticks: {
                        fontColor: 'red',
                    }
                }
            ]
        },
        // annotation: {
        //     annotations: [
                // {
                //     type: 'line',
                //     mode: 'vertical',
                //     scaleID: 'x-axis-0',
                //     value: 'March',
                //     borderColor: 'orange',
                //     borderWidth: 2,
                //     label: {
                //         enabled: true,
                //         fontColor: 'orange',
                //         content: 'LineAnno'
                //     }
                // },
        //     ],
        // },
        //     ],
        // },
    };
    public lineChartColors: Color[] = [
        { // grey
            backgroundColor: 'rgba(254, 254, 254, 1)',
            borderColor: '#755bb7',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)',
        },
        { // dark grey
            backgroundColor: 'rgba(254, 254, 254, 1)',
            borderColor: '#8bc14b',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // red
            backgroundColor: 'rgba(254, 254, 254, 1)',
            borderColor: '#e72d5a',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];
    public lineChartLegend = true;
    public lineChartType = 'line';
    public lineChartPlugins = [pluginAnnotations];

    @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

    constructor() { }

    ngOnInit() {
    }

    // events
    public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
        console.log(event, active);
    }

    public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
        console.log(event, active);
    }


}
