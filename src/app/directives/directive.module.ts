import { NgModule } from '@angular/core';
import { HiddenDirective, setPositionDirective, SetNotificationListHeightDirective, ValidateClickDirective } from '../directives/directive';

@NgModule({

	imports: [
	],
	declarations: [
        HiddenDirective,
		setPositionDirective,
		SetNotificationListHeightDirective,
		ValidateClickDirective
	],
	exports: [
        HiddenDirective,
		setPositionDirective,
		SetNotificationListHeightDirective,
		ValidateClickDirective
	]

})

export class DirectiveModule { }