import { Directive, ElementRef, Renderer, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, Output, EventEmitter } from '@angular/core';

// Directive decorator
@Directive({ selector: '[myHidden]' })
// Directive class
export class HiddenDirective {

    constructor(el: ElementRef, renderer: Renderer) {
        // Use renderer to render the element with styles
        // renderer.setElementStyle(el.nativeElement, 'display', 'none');
    }

}

@Directive({
    selector: '[set-position]'
})
// Directive class
export class setPositionDirective implements AfterViewInit {


    @Input() xCoordinate: any;
    @Input() yCoordinate: any;
    @Input() trackImageProp: any;

    constructor(private el: ElementRef, private renderer: Renderer) { }

    ngAfterViewInit() {
        this.setLeftAndTopOfTrackPoint();
    }

    @HostListener('window:resize', ['$event'])
    onWindowResize(event): void {
        // this.setLeftAndTopOfTrackPoint();
    }

    private setLeftAndTopOfTrackPoint() {

        let element = this.el.nativeElement;
        const { naturalWidth, naturalHeight, clientWidth, clientHeight } = this.trackImageProp;

        let leftPercentage = this.percentage(this.xCoordinate, naturalWidth);
        let topPercentage = this.percentage(this.yCoordinate, naturalHeight);

        this.renderer.setElementStyle(element, 'top', topPercentage + '%');
        this.renderer.setElementStyle(element, 'left', leftPercentage + '%');
    }

    private percentage(partialValue, totalValue) {
        // Math.round(
        return (100 * partialValue) / totalValue;
    }

}

@Directive({
    selector: '[set-notfication-list-height]'
})
// Directive class
export class SetNotificationListHeightDirective implements AfterViewInit {

    @Input() trackImageProp: any;

    constructor(private el: ElementRef, private renderer: Renderer) { }

    ngAfterViewInit() {
        setTimeout(() => {
            this.getNotificationListHeight();
        }, 100);
    }

    @HostListener('window:resize', ['$event'])
    onWindowResize(event): void {
        this.getNotificationListHeight();
    }

    public getNotificationListHeight() {
        let element = this.el.nativeElement;
        const { clientHeight } = this.trackImageProp;
        this.renderer.setElementStyle(element, 'height', (clientHeight + 46) + 'px');
    }

}


@Directive({
    selector: '[validate-click]'
})
// Directive class
export class ValidateClickDirective {

    @Input() clickItemOneClassNameProp: string;
    @Input() clickItemTwoClassNameProp: string;

    @Output() clickedOutsideGiveElementEvent: EventEmitter<any>;

    constructor(private el: ElementRef, private renderer: Renderer) {
        this.clickedOutsideGiveElementEvent = new EventEmitter<any>();
    }

    @HostListener('document:click', ['$event'])
    onClick(event): void {
        let clickedElement = event.target;
        let findClosestItemOneClass = (<any>clickedElement).closest(`.${this.clickItemOneClassNameProp}`);
        let findClosestItemTwoClass = (<any>clickedElement).closest(`.${this.clickItemTwoClassNameProp}`);
        
        if (findClosestItemOneClass || findClosestItemTwoClass) {
        } else {
            this.clickedOutsideGiveElementEvent.emit();
        }

    }

}