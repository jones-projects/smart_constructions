export interface INotificationList {
    notifications: INotification[];
}

export interface INotification {
    eventId?: string;
    jobId?: string;
    lastNotificationMessage?: string;
    lastNotificationTimestamp?: string;
    lastNotificationType?: string;
    lineDescriptor1x?: string;
    lineDescriptor2x?: string;
    lineDescriptor3x?: string;
    lineDescriptor4?: string;
    liveEventImageIcon?: string;
    liveEventNotificationIcon?: string;
    location?: string;
    resourceType?: string;
    xCoordinate?: string;
    yCoordinate?: string;
}

export interface INotificationInfoList {
    notificationsInfo: INotificationInfo[];
}

export interface INotificationInfo {
    personID?: string;
    personName?: string;
    personPic?: string;
    productityScore?: string;
    role?: string;
    safetyScore?: string;
    shift?: string;
    statusID?: string;
    supervisor?: string;
    eventId?: string;
    level?: string;
    pastViolations?: IPastViolation[];
    category?: string;
    currentLocation?: string;
    history?: string;
    siteSupervisor?: string;
    videoImage?: string;
    materialType?: string;
}

export interface IPastViolation {
    action?: string;
    activity1?: string;
    activity2?: string;
    activity3?: string;
    activity4?: string;
    activity5?: string;
    category?: string;
    currentLocation?: string;
    eventId?: string;
    icon?: string;
    jobId?: string;
    level?: string;
    status?: string;
    timeLapsed?: string;
}

export interface ISensorAlertsList {
    Alerts: IAlert[];
}

export interface IAlert {
    alertId?: string;
    deviceType?: string;
    tagId?: string;
    lastAlertMessage?: string;
    lastAlertTimestamp?: string;
    lastNotificationType?: string;
    lineDescriptor1x?: string;
    lineDescriptor2x?: string;
    lineDescriptor3x?: string;
    location?: string;
}


export interface IOperationList {
    Jobs: IJob[];
}

export interface IJob {
    jobDetails?: string;
    jobId?: string;
    tagId?: string;
    jobProgress?: string;
    jobStatus?: string;
    lineDescriptor1x?: string;
    lineDescriptor2x?: string;
    lineDescriptor3x?: string;
    location?: string;
}