import { Component, OnInit, Input, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { INotification, IAlert, IJob } from '../interfaces';
import { HelperService } from '../service/helper.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ListComponent implements OnInit {

    public selectedListItem: any;

    @Input() listHeaderNameProp: string;
    @Input() showExpandBtnProp: boolean;
    @Input() notificationListProp: INotification[];
    @Input() alertListProp: IAlert[];
    @Input() operationListProp: IJob[];
    @Input() other: boolean;
    @Input() headerColorProp:string;
    @Input() dummyListProp: any;
    @Input() isUtilitiesProp: boolean;

    @Input() set selectedNotificationInTrackPointProp(value: INotification) {
        if (value && value.eventId) {
            this.setSelectedListItemAndScroll(value, value.eventId);
        } else {
            this.selectedListItem = '';
        }
    }
    
    @Input() set selectedAlertInTrackPointProp(value: IAlert) {
        if (value && value.alertId) {
            this.setSelectedListItemAndScroll(value, value.alertId);
        } else {
            this.selectedListItem = '';
        }
    }
    
    @Input() set selectedOperationInTrackPointProp(value: IJob) {
        if (value && value.jobId) {
            this.setSelectedListItemAndScroll(value, value.jobId);
        } else {
            this.selectedListItem = '';
        }
    }

    @Output() selectedListItemToShowEvent: EventEmitter<any>;
    @Output() selectedListItemInfoIconEvent: EventEmitter<any>;
    @Output() clickedOutsideListItemEvent: EventEmitter<any>;

    constructor() {
        this.selectedListItem = '';
        this.selectedListItemToShowEvent = new EventEmitter<any>();
        this.selectedListItemInfoIconEvent = new EventEmitter<any>();
        this.clickedOutsideListItemEvent = new EventEmitter<any>();
        this.headerColorProp="";
    }

    ngOnInit() {
    }

    public setSelectedListItemAndScroll(value, valueId) {
        this.selectedListItem = value;
        if (valueId) {
            HelperService.scrollToElement(valueId);
        }
    }

    public handleClickOnListItem(listItem: any, index: number) {
        if (this.showExpandBtnProp) {
            // Expand and collapse list item
            if (this.selectedListItem === listItem) {
                this.selectedListItem = '';
                this.selectedListItemToShowEvent.emit('');             
                return;
            }
            this.selectedListItem = listItem;
            this.selectedListItemToShowEvent.emit(listItem);
        }
    }

    public handleClickOnInfoIcon(event) {
        this.selectedListItemInfoIconEvent.emit(event);
    }

}
