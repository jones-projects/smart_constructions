import { Component, ViewEncapsulation, HostListener, OnInit, Inject } from '@angular/core';
import { INotificationList, INotification, INotificationInfoList, INotificationInfo } from '../interfaces';
import { DOCUMENT } from '@angular/common';
import { AppService } from '../service/app.service';
import { HttpService } from '../service/http.service';

@Component({
    selector: 'app-live.events',
    templateUrl: './live.events.component.html',
    styleUrls: ['./live.events.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LiveEventsComponent implements OnInit {

    private httpService: HttpService;

    public notificationList: INotification[];
    public notificationInfoList: INotificationInfo[];
    public selectedNotificationInfo: INotificationInfo;
    public selectedNotificationInfoType: string;

    public filteredNotificationList: INotification[];
    public dropdownToShow: string;

    public notificationTypeList: any;
    public resourcesTypeList: any;

    public filteredNotificationTypeNameList: string[];
    public selectedNotificationInTrackPoint: INotification;

    public selectedStructureType: string;
    public arrowTypeClass: string;
    public imgURL: string;
    public setArrowPosition: any;
    public document: any;
    public showNotificationDetailsPageType: string;

    constructor(@Inject(DOCUMENT) document, private appService: AppService) {
        this.httpService = HttpService.Instance;
        this.dropdownToShow = '';
        this.selectedStructureType = '';
        this.notificationList = [];
        this.showNotificationDetailsPageType = '';
        this.imgURL = "../../assets/images/banner.jpg";
        this.selectedNotificationInTrackPoint = <INotification>{};
        this.selectedNotificationInfo = <INotificationInfo>{};
        this.selectedNotificationInfoType = '';
        this.notificationInfoList = [];
        this.filteredNotificationTypeNameList = ['Critical', 'Warning', 'Events', 'Personnel', 'Tools', 'Machines', 'Material'];

        this.notificationTypeList = {
            critical: { displayName: 'Critical', actualName: 'Critical' },
            warning: { displayName: 'Warning', actualName: 'Warning' },
            events: { displayName: 'Events', actualName: 'Events' },
        }

        this.resourcesTypeList = {
            personnel: { displayName: 'Personnel', actualName: 'Personnel' },
            tools: { displayName: 'Tools', actualName: 'Tools' },
            machines: { displayName: 'Machines', actualName: 'Machines' },
            material: { displayName: 'Material', actualName: 'Material' },
        }

    }

    ngOnInit() {

        this.httpService.requestPromiseGet('../../assets/json/notifications.json').then((notificationList: INotificationList) => {
            this.appService.setNotificationList(notificationList);
        })

        this.httpService.requestPromiseGet('../../assets/json/notifyInfo.json').then((notificationInfoList: INotificationInfoList) => {
            // this.setNotificationList(notificationList);
            // console.log('notifyInfo::', notificationInfoList);
            this.appService.setNotificationInfoList(notificationInfoList);
        })

        this.appService.getNotificationList().subscribe((notificationList: INotificationList) => {
            this.notificationList = notificationList.notifications;
            this.filteredNotificationList = notificationList.notifications;
        })
        this.appService.getNotificationInfoList().subscribe((notificationInfoList: INotificationInfoList) => {
            this.notificationInfoList = notificationInfoList.notificationsInfo;
            // this.showNotificationDetailsPage = true;
            // this.selectedNotificationInfo = this.notificationInfoList[0];

            // console.log(this.filteredNotificationList);
        })
        this.document = document;
    }

    @HostListener('document:keydown.escape', ['$event'])
    public onKeydownHandler(event: KeyboardEvent) {
        // this.onEscapeEvent.emit('escapeEvent');
        console.log();
    }

    @HostListener('document:click', ['$event'])
    public onClickHandler(event: KeyboardEvent) {
        // this.onEscapeEvent.emit('escapeEvent');
        // This will handle close the list item and track-point based on clicked element.
        let clickedElement = event.target;

        if (clickedElement['className'] === 'drop-down') {
        } else {
            this.dropdownToShow = '';
        }

    }

    public handleCloseSelectedNotification() {
        this.selectedNotificationInTrackPoint = <INotification>{};
    }

    public onChangeNotificationType(event) {
        let elementId = event.target.id;
        let deviceTypeName = this.notificationTypeList[elementId].actualName;
        let elementChecked = event.target.checked;
        this.addOrRemoveNotificationsByName(deviceTypeName);
    }

    public onChangeResourceType(event) {
        let elementId = event.target.id;
        let deviceTypeName = this.resourcesTypeList[elementId].actualName;
        let elementChecked = event.target.checked;
        this.addOrRemoveNotificationsByName(deviceTypeName);
    }

    public onChangeStructureType(event) {
        // event.stopPropagation();
        let elementId = event.target.id;
        this.selectedStructureType = elementId;
        if (elementId == "3dModel") {
            this.imgURL = "../../assets/new icons/3D.jpg";
        }
        else {
            this.imgURL = "../../assets/images/banner.jpg";
        }

    }

    public addOrRemoveNotificationsByName(deviceTypeName: string) {
        let isNotificationTypeNameInArray = this.filteredNotificationTypeNameList.find((deviceType) => deviceType === deviceTypeName);
        if (!isNotificationTypeNameInArray) {
            this.filteredNotificationTypeNameList.push(deviceTypeName);
        } else {
            this.filteredNotificationTypeNameList = this.filteredNotificationTypeNameList.filter((deviceType) => deviceType !== deviceTypeName);
        }
        console.log('filteredNotificationTypeNameList::', this.filteredNotificationTypeNameList);

        this.filteredNotificationList = this.notificationList.filter((notification: INotification) => {
            let indexOfLastNotificationType = this.filteredNotificationTypeNameList.indexOf(notification.lastNotificationType);
            let indexOfLastresourceType = this.filteredNotificationTypeNameList.indexOf(notification.resourceType);
            return (indexOfLastNotificationType !== -1 && indexOfLastresourceType !== -1);
        });

        console.log(this.filteredNotificationList);
    }

    public showTrackPointInfo(targetId: string, notification: INotification) {
        //debugger
        if (notification.eventId !== this.selectedNotificationInTrackPoint.eventId) {
            this.selectedNotificationInTrackPoint = notification;
        } else {
            this.selectedNotificationInTrackPoint = <INotification>{};
        }
        // HelperService.scrollToElement('track_' + notification.eventId);
        console.log(targetId);
        this.setArrowPositionCallFunc(targetId);
    }

    public setArrowPositionCallFunc(targetId: string) {

        let rect = this.document.getElementById(targetId);

        if (rect) {

            rect = this.document.getElementById(targetId).getBoundingClientRect(); //e.target.getBoundingClientRect();  
            if (rect.x < 700 && rect.y < 480) {
                this.arrowTypeClass = "arrow-left";
    
            }
            else if (rect.x > 700 && rect.y < 480) {
                this.arrowTypeClass = "arrow-right";
            }
            else if (rect.y > 480 && rect.x < 700) {
                this.arrowTypeClass = "arrow-bottom";
            }
            else {
                this.arrowTypeClass = "arrow-bottom-right";
            }

        }

    }

    public handleClickOnTrackPointInfo(event) {
        event.stopPropagation();
    }

    public showTrackPointDetailPage() {
        console.log('showTrackPointDetailPage', this.selectedNotificationInTrackPoint);
        this.selectedNotificationInfo = this.matchAndGetNotificationInfo();
        console.log('selectedNotificationInfo::', this.selectedNotificationInfo);
        if (this.selectedNotificationInfo) {
            this.showNotificationDetailsPageType = this.selectedNotificationInTrackPoint.lastNotificationType;
            document.body.className = 'no-scroll';
        } else {
            console.log('there is not notification info');
        }
    }

    public matchAndGetNotificationInfo() {
        return this.notificationInfoList.find((notification) => notification.eventId === this.selectedNotificationInTrackPoint.eventId)
    }

    public handleSelectedListItemInfoIcon(event) {
        event.stopPropagation();
        console.log('clicked on info icon', this.selectedNotificationInTrackPoint);
        this.selectedNotificationInfo = this.matchAndGetNotificationInfo();
        console.log('selectedNotificationInfo::', this.selectedNotificationInfo);
        if (this.selectedNotificationInfo) {
            this.showNotificationDetailsPageType = this.selectedNotificationInTrackPoint.lastNotificationType;
            document.body.className = 'no-scroll';
        } else {
            console.log('there is not notification info');
        }
    }

    public handleCloseNotificationDetailsPage() {
        this.showNotificationDetailsPageType = '';
        document.body.className = '';
        this.selectedNotificationInfo = <INotificationInfo>{};
    }

    public handleSelectedListItemToShow(notification: any) {
        if (notification.eventId !== this.selectedNotificationInTrackPoint.eventId) {
            this.selectedNotificationInTrackPoint = notification;
            console.log(notification.eventId);
            this.setArrowPositionCallFunc('track_' + notification.eventId)
        } else {
            this.selectedNotificationInTrackPoint = <INotification>{};
        }

        // HelperService.scrollToElement('track_' + notification.eventId);
    }

    public showDropdown(dropdownName: string) {
        event.stopPropagation();
        if (this.dropdownToShow === dropdownName) {
            this.dropdownToShow = '';
        } else {
            this.dropdownToShow = dropdownName;
        }
    }

    public handleEscapeEvent($event) {
        this.dropdownToShow = '';
        this.selectedNotificationInTrackPoint = <INotification>{};
    }

    public handleClickEvent($event) {
        this.dropdownToShow = '';
        this.selectedNotificationInTrackPoint = <INotification>{};
    }

}
