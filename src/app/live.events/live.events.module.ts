import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LiveEventsComponent } from './live.events.component';
import { ListModule } from '../list/list.module';
import { OverlayModule } from '../common/overlay/overlay.module';
import { DirectiveModule } from '../directives/directive.module';
import { PipeModule } from '../pipes/pipe.module';
import { NotificationDetailsModule } from './notification.details/notification.details.module';
import { AppService } from '../service/app.service';
import { NotificationNoncriticalModule } from './notification.noncritical/notification.noncritical.module';

@NgModule({

	imports: [
		CommonModule,
		ListModule,
		OverlayModule,
		DirectiveModule,
		PipeModule,
		NotificationDetailsModule,
		NotificationNoncriticalModule,
	],
	declarations: [
		LiveEventsComponent,
	],
	providers: [AppService],
	exports: [
		LiveEventsComponent
	]

})

export class LiveEventsModule { }