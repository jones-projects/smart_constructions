import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { INotificationInfo, IPastViolation } from 'src/app/interfaces';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-notification-details',
    templateUrl: './notification.details.component.html',
    styleUrls: ['./notification.details.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class NotificationDetailsComponent implements OnInit {

    public notificationInfo: INotificationInfo;
    public selectedVoilation: IPastViolation;

    public selectedVoilationActivityList: any[];
    public muteNotificationStatus: boolean;

    public showVoilationActivityImageOrVideo: string;

    @Input() set notificationInfoProp(value: INotificationInfo) {
        this.notificationInfo = value;
        if (value && value.pastViolations && value.pastViolations.length) {
            this.selectedVoilationActivityList = [];
            this.selectedVoilation = value.pastViolations[0];
            this.getActivityList(this.selectedVoilation);
        }
        console.log("via:"+this.selectedVoilation)
    }
    

    @Output() closeNotificationDetailsPageEvent: EventEmitter<any>;


    constructor(public sanitizer: DomSanitizer) {
        this.closeNotificationDetailsPageEvent = new EventEmitter<any>();
        this.selectedVoilation = <IPastViolation> {};
        this.showVoilationActivityImageOrVideo = '';
        this.notificationInfo = <INotificationInfo> {};
        this.selectedVoilationActivityList = [];
        this.muteNotificationStatus = false;
    }

    ngOnInit() {
    }

    public handleCloseNotificationDetailsPage() {
        this.closeNotificationDetailsPageEvent.emit();
    }

    public handleSelectVoilation(violation: IPastViolation) {
        this.selectedVoilation = violation;
        this.selectedVoilationActivityList = [];
        this.getActivityList(this.selectedVoilation);
        console.log("via:"+this.selectedVoilation)
    }

    private getActivityList(violation: IPastViolation) {
        for (const key in violation) {
            if (key.includes('activity')) {
                this.selectedVoilationActivityList.push(violation[key]);
            }
        }
        console.log(this.selectedVoilationActivityList);
    }

    public handlShowMuteNotification() {
        this.muteNotificationStatus = !this.muteNotificationStatus;
    }

    public showViolationActivityImageOrVideo() {
        console.log(this.selectedVoilation);
        this.showVoilationActivityImageOrVideo = this.selectedVoilation.action;
    }

    public closeVoilationActivityImageOrVideo() {
        this.showVoilationActivityImageOrVideo= '';
    }

}
