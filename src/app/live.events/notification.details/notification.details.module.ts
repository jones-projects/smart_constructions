import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationDetailsComponent } from './notification.details.component';

@NgModule({

    imports: [
        CommonModule,
    ],
    declarations: [
        NotificationDetailsComponent
    ],
    exports: [
        NotificationDetailsComponent
    ]

})

export class NotificationDetailsModule { }