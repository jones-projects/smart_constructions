import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { INotificationInfo } from 'src/app/interfaces';

@Component({
    selector: 'app-notification-details-non-critical',
    templateUrl: './notification.noncritical.component.html',
    styleUrls: ['./notification.noncritical.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class NotificationNoncriticalComponent implements OnInit {

    public notificationInfo: INotificationInfo;

    @Input() set notificationInfoProp(value: INotificationInfo) {
        this.notificationInfo = value;
    }

    @Output() closeNotificationDetailsPageEvent: EventEmitter<any>;

    constructor() {
        this.closeNotificationDetailsPageEvent = new EventEmitter<any>();
        this.notificationInfo = <INotificationInfo> {};
    }

    ngOnInit() {
    }

    public handleCloseNotificationDetailsPage() {
        this.closeNotificationDetailsPageEvent.emit();
    }
}
