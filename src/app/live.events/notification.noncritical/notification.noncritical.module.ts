import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationNoncriticalComponent } from './notification.noncritical.component';

@NgModule({

    imports: [
        CommonModule,
    ],
    declarations: [
        NotificationNoncriticalComponent
    ],
    exports: [
        NotificationNoncriticalComponent
    ]

})

export class NotificationNoncriticalModule { }