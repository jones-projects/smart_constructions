import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit {

    public loginCreds: any;
    private username: any;
    private password: any;

    constructor(private router: Router) {
        this.username = '';
        this.password = '';
        this.loginCreds = {
            userName: 'garret.bel',
            password: 'password'
        }
    }

    ngOnInit() {
    }

    public navigateToSecurePage() {

        const { userName, password } = this.loginCreds;

        if (userName === this.username.toLowerCase() && password === this.password.toLowerCase()) {
            this.router.navigate([`/live-events`]);
        } else {
            alert('Please use correct credential');
        }
    }

}
