import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { OverlayModule } from '../common/overlay/overlay.module';

@NgModule({

	imports: [
		CommonModule,
		OverlayModule
	],
	declarations: [
		LoginComponent
	],
	exports: [
		LoginComponent
	]

})

export class LoginModule { }