import { Component, OnInit, EventEmitter, Output, ViewEncapsulation, Input } from '@angular/core';
@Component({
    selector: 'app-master-header',
    templateUrl: './master.header.component.html',
    styleUrls: ['./master.header.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class MasterHeaderComponent implements OnInit {

    @Input() routedTabNameProp: string;
    @Output() routerNameEvent: EventEmitter<string>;
    public showNotificationDetailsPage: boolean;

    constructor() {
        this.routerNameEvent = new EventEmitter<string>();
        this.showNotificationDetailsPage = false;
    }

    ngOnInit() {
    }

    public navigateTo(routeName: string) {
        this.routerNameEvent.emit(routeName);
    }

    public setting() {
       this.showNotificationDetailsPage = true;
    }

    public handleCloseNotificationDetailsPage() {
        this.showNotificationDetailsPage = false;
    }
    
}
