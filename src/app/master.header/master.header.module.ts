import { NgModule } from '@angular/core';
import { MasterHeaderComponent } from "./master.header.component";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SettingModule } from './setting/setting.module';
import { PipeModule } from '../pipes/pipe.module';

@NgModule({

	imports: [
		CommonModule,HttpClientModule,
		SettingModule,PipeModule
	],
	declarations: [
		MasterHeaderComponent
	],
	exports: [
		MasterHeaderComponent
	]

})

export class MasterHeaderModule { }