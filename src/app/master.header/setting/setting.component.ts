import { Component, OnInit, EventEmitter, Output, ViewEncapsulation, Input } from '@angular/core';
@Component({
    selector: 'app-setting',
    templateUrl: './setting.component.html',
    styleUrls: ['./setting.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SettingComponent implements OnInit {

    @Output() closeNotificationDetailsPageEvent: EventEmitter<any>;

    constructor() {
        this.closeNotificationDetailsPageEvent = new EventEmitter<any>();
    }

    ngOnInit() {
    }

    public handleCloseNotificationDetailsPage() {
        this.closeNotificationDetailsPageEvent.emit();
    }

    public saveSettings() {
        this.closeNotificationDetailsPageEvent.emit();
    }

}
