import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SettingComponent } from './setting.component';
import { PipeModule } from 'src/app/pipes/pipe.module';

@NgModule({

	imports: [
        CommonModule,HttpClientModule,
        PipeModule
	],
	declarations: [
		SettingComponent
	],
	exports: [
		SettingComponent
	]

})

export class SettingModule { }