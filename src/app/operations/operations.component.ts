import { Component, OnInit, ViewEncapsulation, HostListener, Inject } from '@angular/core';
import { HttpService } from '../service/http.service';
import { IOperationList, IJob } from '../interfaces';
import { DOCUMENT } from '@angular/common';

@Component({
    selector: 'app-operations',
    templateUrl: './operations.component.html',
    styleUrls: ['./operations.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class OperationsComponent implements OnInit {

    private httpService: HttpService;
    public operationList: IJob[];
    public selectedOperationInTrackPoint: IJob;
    public showShiftReport: boolean;
    public arrowTypeClass: string;
    public document: any;
    public showInfo: boolean;
    constructor(@Inject(DOCUMENT) document) {
        this.showShiftReport = false;
        this.showInfo = false;
        this.operationList = [];
        this.selectedOperationInTrackPoint = <IJob>{};
        this.httpService = HttpService.Instance;
        this.httpService.requestPromiseGet('../../assets/json/operations.json').then((operations: IOperationList) => {
            this.operationList = operations.Jobs;
        });
        this.document = document;
    }

    ngOnInit() {
    }

    @HostListener('document:keydown.escape', ['$event'])
    public onKeydownEscapeHandler(event: KeyboardEvent) {
        this.closeShiftReport();
    }

    public showTrackPointInfo(targetId: string, job: IJob) {
        event.stopPropagation();
        this.selectedOperationInTrackPoint = job;
        this.setArrowPositionCallFunc(targetId);
    }

    public setArrowPositionCallFunc(targetId: string) {

        let rect = this.document.getElementById(targetId);

        if (rect) {
            rect = this.document.getElementById(targetId).getBoundingClientRect(); //e.target.getBoundingClientRect();  
            if (rect.x < 700 && rect.y < 480) {
                this.arrowTypeClass = "arrow-left";
    
            }
            else if (rect.x > 700 && rect.y < 480) {
                this.arrowTypeClass = "arrow-right";
            }
            else if (rect.y > 480 && rect.x < 700) {
                this.arrowTypeClass = "arrow-bottom";
            }
            else {
                this.arrowTypeClass = "arrow-bottom-right";
            }

        }

    }
    public handleSelectedListItemToShow(job: any) {

        this.selectedOperationInTrackPoint = job;
        this.setArrowPositionCallFunc('track_' + job.jobId)
    }

    public handleClickShiftReport() {
        this.showShiftReport = true;
        document.body.style.overflow = 'hidden';
    }

    public closeShiftReport() {
        this.showShiftReport = false;
        document.body.style.overflow = 'unset';
    }

    public handleCloseSelectedOperation() {
        this.selectedOperationInTrackPoint = <IJob> {};
    }

    public handleEscapeEvent($event) {
        this.selectedOperationInTrackPoint = <IJob>{};
    }

    public handleClickEvent($event) {
        this.selectedOperationInTrackPoint = <IJob>{};
    }

    public showTrackPointDetailPage() {
        console.log('calling');
        this.showInfo = true;
        document.body.className = 'no-scroll';
    }

    public handleCloseNotificationDetailsPage() {
        this.showInfo = false;
        document.body.className = '';
    }

    public handleSelectedListItemInfoIcon(event) {
        event.stopPropagation();
        console.log(event)
        this.showInfo = true;
        document.body.className = 'no-scroll';
        // console.log('clicked on info icon', this.selectedNotificationInTrackPoint);
        // this.selectedNotificationInfo = this.matchAndGetNotificationInfo();
        // if (this.selectedNotificationInfo) {
        //     this.showNotificationDetailsPage = true;
        // } else {
        //     console.log('there is not notification info');
        // }
    }
}
