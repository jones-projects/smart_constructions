import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperationsComponent } from './operations.component';
import { ListModule } from '../list/list.module';
import { DirectiveModule } from '../directives/directive.module';
import { OverlayModule } from '../common/overlay/overlay.module';
import { PipeModule } from '../pipes/pipe.module';

@NgModule({

	imports: [
		CommonModule,
		ListModule,
		DirectiveModule,
		OverlayModule,
		PipeModule,
	],
	declarations: [
		OperationsComponent
	],
	exports: [
		OperationsComponent
	]

})

export class OperationsModule { }