import { NgModule } from '@angular/core';
import {
    SetOverlayHeightPipe,
    SetListHeightPipe,
    CoordinateToPercentagePipe
} from './pipe';


const PIPE_LIST = [
    SetOverlayHeightPipe,
    SetListHeightPipe,
    CoordinateToPercentagePipe
];

@NgModule({
    declarations: [
        PIPE_LIST
    ],
    exports: [
        PIPE_LIST
    ],
    providers: [
    ]
})
export class PipeModule { }
