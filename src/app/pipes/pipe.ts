import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'setOverlayHeight',
    pure: true
})
export class SetOverlayHeightPipe implements PipeTransform {
    constructor() { }

    transform() {
        let clientHeight = document.body.clientHeight;
        return clientHeight;
    }
}

@Pipe({
    name: 'setListHeight'
})
export class SetListHeightPipe implements PipeTransform {

    constructor() { }

    transform(filterList) {
        setTimeout(() => {
            return (filterList + 46);    
        }, 100);
        return (filterList + 46);
    }

}

@Pipe({
    name: 'coordinateToPercentage'
})
export class CoordinateToPercentagePipe implements PipeTransform {

    constructor() { }

    transform(coordinateValue, trackImageValue) {
        setTimeout(() => {
            return this.percentage(coordinateValue, trackImageValue);
        }, 100);
        return this.percentage(coordinateValue, trackImageValue);
    }

    private percentage(partialValue, totalValue) {
        return (100 * partialValue) / totalValue;
    }

}
