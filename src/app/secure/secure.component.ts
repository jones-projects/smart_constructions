import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AppService } from '../service/app.service';

@Component({
    selector: 'app-secure',
    templateUrl: './secure.component.html',
    styleUrls: ['./secure.component.scss']
})
export class SecureComponent implements OnInit {

    private routedTabName: string;

    constructor(private router: Router, private appService: AppService ) {
        this.routedTabName = '';
        this.router.events.subscribe(this.routerChangelistener.bind(this));
    }

    ngOnInit() {
    }

    private routerChangelistener(event: Event) {
        if (event instanceof NavigationEnd) {
            const { url } = event;
            // let moduleName = url.split('/')[1];
            let moduleName = url.substr(1);
            if (moduleName) {
                this.routedTabName = moduleName;
            }
        }
    }

    public handleRouterNameEvent(routeName: string) {
        this.router.navigate([`/${routeName}`]);
    }

}
