import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecureComponent } from './secure.component';
import { MasterHeaderModule } from '../master.header/master.header.module';
import { AppService } from '../service/app.service';

@NgModule({

    imports: [
        CommonModule,
        MasterHeaderModule
    ],
    declarations: [
        SecureComponent
    ],
    providers: [AppService],
    exports: [
        SecureComponent
    ]

})

export class SecureModule { }