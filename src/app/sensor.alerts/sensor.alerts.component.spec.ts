import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorAlertsComponent } from './sensor.alerts.component';

describe('Sensor.AlertsComponent', () => {
    let component: SensorAlertsComponent;
    let fixture: ComponentFixture<SensorAlertsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SensorAlertsComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SensorAlertsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
