import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { HttpService } from '../service/http.service';
import { ISensorAlertsList, IAlert } from '../interfaces';
import { DOCUMENT } from '@angular/common';


@Component({
    selector: 'app-sensor.alerts',
    templateUrl: './sensor.alerts.component.html',
    styleUrls: ['./sensor.alerts.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SensorAlertsComponent implements OnInit {

    private httpService: HttpService;
    public alertList: IAlert[];
    public filteredAlertList: IAlert[];
    public filteredDeviceTypeNameList: string[];
    public deviceTypeList: any;

    public selectedAlertInTrackPoint: IAlert;

    public arrowTypeClass: string;
    public document: any;

    constructor(@Inject(DOCUMENT) document) {
        this.alertList = [];
        this.selectedAlertInTrackPoint = <IAlert>{};
        this.filteredDeviceTypeNameList = ['Safety Vest', 'Realwear Device', 'Camera', 'Asset Tag', 'Drone', 'Lidars'];
        this.deviceTypeList = {
            safetyVest: { displayName: 'Safety Vest', actualName: 'Safety Vest' },
            realwearDevice: { displayName: 'Realwear Device', actualName: 'Realwear Device' },
            camera: { displayName: 'Cameras', actualName: 'Camera' },
            assetTag: { displayName: 'Asset Tags', actualName: 'Asset Tag' },
            drone: { displayName: 'Drones', actualName: 'Drone' },
            lidars: { displayName: 'Lidars', actualName: 'Lidars' }
        }
        // this.deviceTypeList = ['Safety Vest', 'Realwear Device', 'Camera', 'Asset Tag', 'Drone', 'Lidars'];
        this.httpService = HttpService.Instance;
        this.httpService.requestPromiseGet('../../assets/json/sensor-alerts.json').then((sensorAlerts: ISensorAlertsList) => {
            console.log('sensorAlerts::', sensorAlerts);
            this.alertList = sensorAlerts.Alerts;
            this.filteredAlertList = sensorAlerts.Alerts;
        });
        this.document = document;
    }

    ngOnInit() {
    }

    public onChangeDeviceType(event) {
        let elementId = event.target.id;
        let deviceTypeName = this.deviceTypeList[elementId].actualName;
        let elementChecked = event.target.checked;
        this.addOrRemoveDeviceTypeByName(deviceTypeName);
    }

    private addOrRemoveDeviceTypeByName(deviceTypeName: string) {

        // add or remove deviceType from the filteredDeviceTypeNameList
        let isDeviceTypeNameInArray = this.filteredDeviceTypeNameList.find((deviceType) => deviceType === deviceTypeName);
        if (!isDeviceTypeNameInArray) {
            this.filteredDeviceTypeNameList.push(deviceTypeName);
        } else {
            this.filteredDeviceTypeNameList = this.filteredDeviceTypeNameList.filter((deviceType) => deviceType !== deviceTypeName);
        }

        this.filteredAlertList = this.alertList.filter((alert: IAlert) => {
            return (this.filteredDeviceTypeNameList.indexOf(alert.deviceType) !== -1);
        });

        console.log(this.filteredAlertList);
    }

    public showTrackPointInfo(targetId: string, alert: IAlert) {
        this.selectedAlertInTrackPoint = alert;
        this.setArrowPositionCallFunc(targetId);
    }

    public setArrowPositionCallFunc(targetId: string) {

        let rect = this.document.getElementById(targetId);

        if (rect) {
            rect = this.document.getElementById(targetId).getBoundingClientRect(); //e.target.getBoundingClientRect();  
            if (rect.x < 700 && rect.y < 480) {
                this.arrowTypeClass = "arrow-left";
            } else if (rect.x > 700 && rect.y < 480) {
                this.arrowTypeClass = "arrow-right";
            } else if (rect.y > 480 && rect.x < 700) {
                this.arrowTypeClass = "arrow-bottom";
            } else {
                this.arrowTypeClass = "arrow-bottom-right";
            }
        }

    }

    public handleSelectedListItemToShow(alert: any) {
        this.selectedAlertInTrackPoint = alert;
        this.setArrowPositionCallFunc('track_' + alert.alertId)
    }

    public handleEscapeEvent($event) {
        this.selectedAlertInTrackPoint = <IAlert>{};
    }

    public handleCloseSelectedAlert() {
        this.selectedAlertInTrackPoint = <IAlert>{};
    }

    public handleClickEvent($event) {
        this.selectedAlertInTrackPoint = <IAlert>{};
    }

}