import { HttpService } from './http.service';
import { INotificationList, INotificationInfoList } from '../interfaces';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class AppService {

    private httpService: HttpService;
    public observeNotificationList: Subject<INotificationList>;
    public observeNotificationInfoList: Subject<INotificationInfoList>;

    constructor() { 
        this.observeNotificationList = new Subject<INotificationList>();
        this.observeNotificationInfoList = new Subject<INotificationInfoList>();
        this.httpService = HttpService.Instance;
        this.init();
    }

    private init() {
        this.httpService.requestPromiseGet('../../assets/json/notifications.json').then((notificationList: INotificationList) => {
            this.setNotificationList(notificationList);
        })

        this.httpService.requestPromiseGet('../../assets/json/notifyInfo.json').then((notificationInfoList: INotificationInfoList) => {
            // this.setNotificationList(notificationList);
            console.log('notifyInfo::', notificationInfoList);
            this.setNotificationInfoList(notificationInfoList);
        })
    }

    public setNotificationList(notificationList: INotificationList) {
        this.observeNotificationList.next(notificationList);
    }

    public getNotificationList(): Subject<INotificationList> {
        return this.observeNotificationList;
    }
    
    
    
    public setNotificationInfoList(notificationInfoList: INotificationInfoList) {
        this.observeNotificationInfoList.next(notificationInfoList);
    }

    public getNotificationInfoList(): Subject<INotificationInfoList> {
        return this.observeNotificationInfoList;
    }


}