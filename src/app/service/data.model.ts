export class DataModel {

    private static instance: DataModel;

    constructor() { }

    public static get Instance() {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new DataModel();
        }
        return this.instance;
    }

    

}