import { IJob } from '../interfaces';

var keyAndValues = {
    'Notifications': 'notifications',
    'Event ID': 'eventId',
    'Location': 'location',
    'Line Descriptor 1': 'lineDescriptor1x',
    'Line Descriptor 2': 'lineDescriptor2x',
    'Line Descriptor 3': 'lineDescriptor3x',
    'ResourceType': 'resourceType',
    'JOB ID': 'jobId',
    'Line Descriptor 4': 'lineDescriptor4',
    'liveEventNotificationIcon': 'liveEventNotificationIcon',


    'Notificationsinfo': 'notificationsInfo',
    'Level': 'level',
    'Past Violations': 'pastViolations',
    'Person ID': 'personID',
    'Person Name': 'personName',
    'Person Pic': 'personPic',
    'Productity Score': 'productityScore',
    'Role': 'role',
    'Safety Score': 'safetyScore',
    'Shift': 'shift',
    'Status_ID': 'statusID',
    'Supervisor': 'supervisor',
    'History': 'history',
    'Site Supervisor': 'siteSupervisor',
    'Video_Image': 'videoImage',

    'Action': 'action',
    'Activity 1': 'activity1',
    'Activity 2': 'activity2',
    'Activity 3': 'activity3',
    'Activity 4': 'activity4',
    'Activity 5': 'activity5',
    'Category': 'category',
    'Current Location': 'currentLocation',
    'Icon': 'icon',
    'Status': 'status',
    'Time Lapsed': 'timeLapsed',


    'Alert ID': 'alertId',
    'DeviceType': 'deviceType',
    'Tag ID': 'tagId',

    'Job ID': 'jobId',
    'JobDetails': 'jobDetails',
    'JobProgress': 'jobProgress',
    'JobStatus': 'jobStatus',

    'x-coordinate': 'xCoordinate',
    'y-coordinate': 'yCoordinate',
}

export class HelperService {

    public static isJSON(data: any) {
        try {
            JSON.parse(data);
        } catch (e) {
            return false;
        }
        return true;
    }

    public static processedXHRResponse(xhr: XMLHttpRequest) {
        if (this.isJSON(xhr.responseText)) {
            let jsonResponse = JSON.parse(xhr.responseText);

            let recurse = (obj, current?) => {
                for (const key in obj) {
                    let value = obj[key];
                    if (value != undefined) {

                        let newKey = keyAndValues[key] || key;

                        // console.log('newKey::', newKey);

                        if (value && typeof value === 'object') {

                            if (isNaN(<any>newKey) && newKey !== key) {
                                obj[newKey] = obj[key];
                                delete obj[key];
                                recurse(obj[newKey]);
                            } else {
                                recurse(obj[key]);
                            }

                        } else {
                            if (newKey !== key) {
                                obj[newKey] = obj[key];
                                delete obj[key];
                            }
                        }
                    }
                }
            }
            recurse(jsonResponse);

            return jsonResponse;
        }
    }


    public static scrollToElement(id: string) {
        let el = document.getElementById(id);
        el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }

}