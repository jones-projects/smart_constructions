import { Component, OnInit,Input } from '@angular/core';

@Component({
    selector: 'app-utilization',
    templateUrl: './utilization.component.html',
    styleUrls: ['./utilization.component.scss']
})
export class UtilizationComponent implements OnInit {
 
    public topIncidents: any[];

    constructor() {
        this.test();
        this.topIncidents = [
            {
                'id': 1,
                'details': 'Pipe Leakage',
                'type': '',
                'utilizationNotificationIcon': 'Pipe Leakage.svg'
            }, {
                'id': 2,
                'details': 'Equipment Not Found',
                'type': '',
                'utilizationNotificationIcon': 'Equipment Not Found.svg'
            },
            {
                'id': 3,
                'details': 'Material Not Available',
                'type': '',
                'utilizationNotificationIcon': 'Material Not Available.svg'
            },
            {
                'id': 4,
                'details': 'Excavation Delayed',
                'type': '',
                'utilizationNotificationIcon': 'Excavation Delayed.svg'
            },
            {
                'id': 5,
                'details': 'Health Risk Detected',
                'type': '',
                'utilizationNotificationIcon': 'Health Risk Detected.svg'
            }
        ];

     }

    ngOnInit() {
    }


    public test() {
        // var ctx = document.getElementsByTagName('canvas');
        // console.log(ctx);

        var img = new Image();

        img.onload = function() {
            var ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
            console.log(ctx);
        }
    }

}
