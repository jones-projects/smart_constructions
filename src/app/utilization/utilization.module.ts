import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilizationComponent } from './utilization.component';
import { BarChartModule } from '../common/bar.chart/bar.chart.module';
import { ListModule } from '../list/list.module';
import { ProgressChartModule } from '../common/progress.chart/progress.chart.module';
@NgModule({

	imports: [
		CommonModule,
		BarChartModule,
		ListModule,
		ProgressChartModule
	],
	declarations: [
		UtilizationComponent
	],
	exports: [
		UtilizationComponent
	]

})

export class UtilizationModule { }